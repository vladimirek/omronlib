#ifndef PASSWDLG_H
#define PASSWDLG_H

#include <QDialog>

namespace Ui {
    class PasswDlg;
}

class PasswDlg : public QDialog
{
    Q_OBJECT

public:
    explicit PasswDlg(QWidget *parent = 0);
    ~PasswDlg();

    QString getPasswd();
private:
    Ui::PasswDlg *ui;
};

#endif // PASSWDLG_H
