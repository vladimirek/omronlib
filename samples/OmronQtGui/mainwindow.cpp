#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileInfo>
#include <QCoreApplication>

#include "passwdlg.h"

#define VERSION "0.9.0"
#define PLC_IP_1 "localhost"
#define NOT_FOR_PRODUCTION 1


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //Init pointers
    plc1Proxy = NULL;
    translator = NULL;

    disable_shutdown = false;

    //Connect to plc after gui is up and running
    QTimer::singleShot(10, this, SLOT(initMainWindow()));
    timerPer1000 = new QTimer();

    //restart timer when comm fail event
    plc1RestartTimer = new QTimer( );
    plc1RestartTimer->setInterval( 3000);
    QObject::connect( plc1RestartTimer, SIGNAL(timeout()), this, SLOT(plc1Restart()));

}

MainWindow::~MainWindow()
{
    plc1Proxy->stopProxy();
    delete plc1Proxy; plc1Proxy = NULL;

    delete ui;
}

void MainWindow::initMainWindow()
{
    //GUI itself
    translateToSlovak();

    ui->l_version->setText( QString(VERSION));

    //translate menu
    QObject::connect( ui->actionTo_English, SIGNAL(triggered()), this, SLOT(transtateToEnglish()));
    QObject::connect( ui->actionTo_Slovak, SIGNAL(triggered()), this, SLOT(translateToSlovak()));


    QObject::connect( ui->actionLogin, SIGNAL(triggered()), this, SLOT(adminLogin()));
    QObject::connect( ui->actionLogout, SIGNAL(triggered()), this, SLOT(adminLogout()));

    plc1Proxy = new PlcProxy( PLC_IP_1);

    //register for read errors from proxy
    QObject::connect( plc1Proxy, SIGNAL(error(PlcProxy::EplcProxyError)), this, SLOT(plc1ProxyError(PlcProxy::EplcProxyError)));

    initPlc1WidgetsStructure(); //setup notifiers
    startPlc1Comm();

    //periodic called functions (state machine ..., plc alive)
    timerPer1000->start( 1000);
    QObject::connect( timerPer1000, SIGNAL(timeout()), this, SLOT(periodic1000ms()));

    //main tab page is changed
    QObject::connect( ui->mainTabWgt, SIGNAL(currentChanged(int)), this, SLOT(mainTabChanged(int)));

    checkFullScreen();
}

void MainWindow::startPlc1Comm()
{
    if( plc1Proxy) {

        plc1Proxy->flushRegions(); //delete regions registrations
        //plcProxy->disableWrite();

        //Main PLC
        plc1Proxy->addRegion("H0",100,200);
        plc1Proxy->addRegion("H100",100,200);
        plc1Proxy->addRegion("D10000",100,1000);

        //read all values from PLC
        plc1Proxy->startProxy( true); //force notify - init whole gui
        plc1Proxy->stopProxy();
        plc1Proxy->startProxy();
        plc1Proxy->enableWrite();

    } else {
        qCritical() << "PLC proxy object not found ..";
    }
}

void MainWindow::initPlc1WidgetsStructure()
{
    qDebug() << "MainWindow::initPlcWidgetsStructure";
    plc1EventHandler.Init( plc1Proxy);    //inputs

    widget1EventHandler.Init( plc1Proxy); //outputs

    PlcEvent *pa;

    PlcWidgetSender *pws;

    QString trueStyle  = "background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #80db80, stop: 1 #90fb90);";
    QString falseStyle = "background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0 #db8080, stop: 1 #fb8080);";


    //HMI - indikators - change vissibility from PLC:
    plc1EventHandler.Add( "H10:1", new PlcEvent_QWidget(PlcEvent::VISIBLE_IF_TRUE,  ui->l_h10_1_s1));


    //string formater for labels
    QTextStream *sf = new QTextStream( new QString());
    sf->setRealNumberPrecision( 1);
    sf->setRealNumberNotation( QTextStream::FixedNotation);

    QTextStream *sr = new QTextStream( new QString());
    sr->setRealNumberPrecision( 0);
    sr->setRealNumberNotation( QTextStream::FixedNotation);

    //HMI - indikators - numbers form PLC to labels:
    plc1EventHandler.Add( "H20", new PlcEvent_QLabel( PlcEvent::UPDATE_VALUE_FLOAT, ui->l_h20f_s3, sf, 2));
    plc1EventHandler.Add( "H19", new PlcEvent_QLabel( PlcEvent::UPDATE_VALUE_UNSIG, ui->l_h19w_s2, sr, 1));
    plc1EventHandler.Add( "H44", new PlcEvent_QLabel( PlcEvent::UPDATE_VALUE_BCD, ui->l_h44w_sd, sr, 1));

    //HMI - controls - set to one if clicked
    pws = new PlcWidgetSender( "H105:2",  ui->pb_h16_3_sd,       plc1Proxy); pws->OneIfClicked();
    pws = new PlcWidgetSender( "H109:0",  ui->pb_h109_0_off_s4,  plc1Proxy); pws->ZeroIfClicked();

    //HMI - controls - spinbox
    widget1EventHandler.Add( "H40", new SpinboxPlcSender(
               ui->sb_h40_sa, trUtf8("L1_Sno"),ui->sb_h40_sa),
               PlcProxy::TYPE_UWORD );

    plc1EventHandler.Add( "D10002", new PlcEvent_QDoubleSpinBox(PlcEvent::UPDATE_VALUE_FLOAT, ui->sb_d10002f_sd, 2)); //readback from plc
    widget1EventHandler.Add( "D10002", new SpinboxPlcSender(
               ui->sb_d10002f_sd, trUtf8("L1_W"),ui->sb_d10002f_sd),
               PlcProxy::TYPE_FLOAT_2W );


    //Finnaly:
    //register readback signal from Proxy to this object
    QObject::connect( this->plc1Proxy, SIGNAL(readed(QString,QList<u16>)),&plc1EventHandler,SLOT(plcEventService(QString,QList<u16>)));
}

void MainWindow::plc1ProxyError(PlcProxy::EplcProxyError error)
{
    qDebug() << "PLC ERROR" << error;

    if( !plc1RestartTimer->isActive()) {
        qDebug() << " ...Start Timer";
        plc1RestartTimer->start( 3000);
    }
}

void MainWindow::plc1Restart()
{
    qDebug() << "PLC RESTART";
    plc1RestartTimer->stop();    
    plc1Proxy->stopProxy();
    startPlc1Comm();
}

void MainWindow::changeEvent(QEvent *e)
{
    if( e->type() == QEvent::LanguageChange) {
        ui->retranslateUi( this);
    }
    QMainWindow::changeEvent( e);
}


void MainWindow::translateTo(QString lang)
{
    //It is single translator instance in application,
    //it is installed in constructor.
    //In this case, when load failed default language is used,
    //not the last one

    qDebug() << "Translate request" << lang;

    //remove previous installed translator
    if( translator) {
        QApplication::removeTranslator( translator);
        delete translator; translator = NULL;
    }

    QTranslator *new_translator = new QTranslator(this);
    QString appDir =QFileInfo( QCoreApplication::applicationFilePath() ).absolutePath();
    bool s = new_translator->load(QString("Lang_") + lang, appDir);
    if( !s) {
        //native language not found
        qDebug() << QObject::trUtf8("Native Language Not found");
        delete new_translator;
    } else {
        translator = new_translator;
        QApplication::installTranslator( translator);
    }
    //ui->retranslateUi( this);
}


void MainWindow::adminLogin()
{
    PasswDlg dlg(this);
    if( dlg.exec() == QDialog::Accepted) {
        QString passwd = dlg.getPasswd();
        if( passwd == QString("1234")) {
            //ui->mainTabWgt->setTabEnabled( 1, true);
            //ui->mainTabWgt->setTabEnabled( 2, true);

            disable_shutdown = true;
            this->setWindowState( Qt::WindowNoState);

        }
    }
}

void MainWindow::adminLogout()
{
    ui->mainTabWgt->setTabEnabled( 1, false);
    ui->mainTabWgt->setTabEnabled( 2, false);
    disable_shutdown = false;
    this->setWindowState( Qt::WindowFullScreen);
}

#include <QProcess>
void MainWindow::shutMeDown()
{
    if( !disable_shutdown) {
        //windows only
        QString program = "shutdown";
        QStringList arguments;
        arguments << "/s" << "/t" << "6";

        QProcess *myProcess = new QProcess();
        myProcess->start(program, arguments);
        myProcess->waitForStarted( 2000);

    }
    QTimer::singleShot(1000,this,SLOT(close()));

}

void MainWindow::checkFullScreen()
{
#ifdef NOT_FOR_PRODUCTION
    return;
#endif
    if( !disable_shutdown) {
        this->show();
        this->raise();
        this->activateWindow();
        this->setWindowState( Qt::WindowNoState);
        this->setWindowState( Qt::WindowFullScreen);
    }
}

void MainWindow::periodic1000ms()
{
    qDebug() << "Periodic ...";
    //send PC is alive signal to PLC
    //plc1Proxy->writeData("H100:15",0x01); //PC alive
}

void MainWindow::mainTabChanged(int idx)
{
    if( idx == 1) {
        //
    } else {
        //
    }

}
