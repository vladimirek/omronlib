#include "passwdlg.h"
#include "ui_passwdlg.h"

PasswDlg::PasswDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PasswDlg)
{
    ui->setupUi(this);
    ui->lineEdit->setFocus();
}

PasswDlg::~PasswDlg()
{
    delete ui;
}

QString PasswDlg::getPasswd()
{
    return ui->lineEdit->text();
}
