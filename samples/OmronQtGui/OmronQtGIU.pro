#-------------------------------------------------
#
# Project created by QtCreator 2016-02-08T13:42:02
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets network

TARGET = OmronQtGIU
TEMPLATE = app

INCLUDEPATH += ../../plcQlib/

SOURCES += main.cpp\
        mainwindow.cpp \
    passwdlg.cpp

HEADERS  += mainwindow.h \
    passwdlg.h

FORMS    += mainwindow.ui \
    passwdlg.ui



# Include PLC library -------BEGIN
SOURCES += \
    ../../plcQlib/widgetplcinterface.cpp \
    ../../plcQlib/plcwidgetsender.cpp \
    ../../plcQlib/plcwidgeteventhandler.cpp \
    ../../plcQlib/plcspinboxsender.cpp \
    ../../plcQlib/plcproxy.cpp \
    ../../plcQlib/plcevhandler.cpp \
    ../../plcQlib/plcevent.cpp \
    ../../plcQlib/omronfinscom.cpp \
    ../../plcQlib/numeditdialog.cpp \
    ../../plcQlib/cdoublespinbox.cpp


HEADERS  += \
    ../../plcQlib/widgetplcinterface.h \
    ../../plcQlib/types.h \
    ../../plcQlib/plcwidgetsender.h \
    ../../plcQlib/plcwidgeteventhandler.h \
    ../../plcQlib/plcspinboxsender.h \
    ../../plcQlib/plcqlib.h \
    ../../plcQlib/plcproxy.h \
    ../../plcQlib/plcevhandler.h \
    ../../plcQlib/plcevent.h \
    ../../plcQlib/omronfinscom.h \
    ../../plcQlib/numeditdialog.h \
    ../../plcQlib/cdoublespinbox.h

FORMS    += \
    ../../plcQlib/numeditdialog.ui
# Include PLC library ------- END
